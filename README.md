Jim Varney emoji set! Emojis are 500x500, cropped from an image from [this reddit post](https://www.reddit.com/r/Damnthatsinteresting/comments/jcgbz9/assorted_expressions_of_jim_varney_aka_ernest_p).

You can download the .ZIP here:

|                         |                           |                          |                             |
| ----------------------- | ------------------------- | ------------------------ | --------------------------- |
| ![](/images/varneyWow.png)     | ![](/images/varneySad.png)       | ![](/images/varneyMad.png)      | ![](/images/varneyUnconvinced.png) |
| ![](/images/varneyManic.png)   | ![](/images/varneyTired.png)     | ![](/images/varneyConfused.png) | ![](/images/varneySuspicious.png)  |
| ![](/images/varneySpooked.png) | ![](/images/varneyConfident.png) | ![](/images/varneySleepy.png)   | ![](/images/varneyDoubtful.png)    |
| ![](/images/varneyCurious.png) | ![](/images/varneyNope.png)      | ![](/images/varneyShock.png)    | ![](/images/varneyNervous.png)     |
